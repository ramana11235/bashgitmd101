# Linux Shell Document

This is a document denoting basic linux commands and their functions. This also includes information on commands used within a server `ssh`, shell directives (including wildcards), and the order of execution operation when a command is run.

---

## Sections

1. Shell Commands
2. The Timeline of Operations When A Command Is Run
3. Wildcards Commands and Examples 
4. Redirection Commands and Examples

---

## Note On Formatting

Shell commands (e.g. `cmd`) are denoted in `this` format.

### Example Format:

`cmd1` - Description of Command  1

---

## Shell commands

- `ssh ramana@linux.academy.grads.al-labs.co.uk` - To login through SSH
- `who` - To see who has logged on:
- `ls -la`: List information with "long all" option for a lot more information

    ### **Important To Remember Operation Execution Order**
    - `command` + `option(s)` + `argument` 
    - For example `ls -i /etc/` is broken up as `ls` + `-i` + `/etc/`
        - This translates to `list the contents` + `including the node number` + `in the directory /etc/`
    - This order can be manipulated

---

## The Timeline of Operations When A Command Is Run

1. A `command` is written in the Shell and Executed.
   -   For example, the command line: `ls -i /etc/`
2. The command line is separated into arrays based on the spacing on the command line 
   - The command line is read into separately as `ls` + `i` + `/etc/`
3. Each element is checked to see if it is a shell directive.
   - A shell directive is completed before the command is executed. These are:
     - Wildcards 
       - `*`, `p` `[ ]`, `?`
       - These are filenames generators

     - Quotes 
       - `''`, `""`, `\`

     - Subshell 
       - `$cmd $(cmd)`
       - Runs a "subcommand" for the main command

     - Redirections 
       - `>`, `>>`, `2>`, `2>>`, `<`

     - Pipelines 
       - `cmd | cmd`
       - Command on the right becomes input for command on the right


     - Variables
       -  `$PATH`


   - The shell expands these directives and sets it up as variables for the cmd to execute on
4. It will then process each directive
5. As it process each directive it rewrites the command line 
6. Then it searches through $PATH looking for where the actual command is
7. It will then run the command based on it's absolute path
8. Replaces the code for the shell with the code of the command
   - Overwrites the bash function in the memory with the command 
--- 
## Wildcards Commands (`*`, `p`, `[]`, `?`) and Examples


- `ls -d /etc/p*` 
  - Show me all files *STARTING* with `p` 
    - the `-d` stops the directory from showing the contents and show it as a folder
    - `-i` is the i-node number, a unique number for every file/folder

- `ls -d /etc/*d` 
  - Show me all files *ENDING* with `d`
- `ls -d /etc/*z*` 
  - Show me all files including a `z`


- `ls -d /etc/[aeiou]*`
  - Show me all files *starting* with a *vowel* 
- `ls -d /etc/*[aeiou]`
  - Show me all files *ending* with a *vowel*


- `ls -d /etc/[A-Z]*` 
  - Show me all files starting with an *upper case*
  - Sometimes need to run command `export LANG=C` to convert shell language to ASCII for above cmd to be possible


- `ls -d /etc/[A-Z0-9]*` 
  - Show me all files starting with a *capital letter and a number*? (NEED TO CONFIRM) 
- `ls -d /etc/????` to searach for file names with 5 characters
- if `ls` for some reason died
  - you can use `echo /etc/p*` to print out the files
  - echo prints the expanded of `/etc/p*`

- You can use `echo` before any command to see what the command being run will be because the echo would print it out 


- copy is `cp a acopy` copies a as acopy
- but `cp a b x` then files a and b gets copied into x
- `|` pipe takes the standard output and switches as an input for another comand
- `cd ~` always take you to `/home/ramana/`
- `cd /` always goes to the root folder

--- 

## Redirection Commands (> >> 2> 2>> <) and Examples

- Get's information as a program


- `>` redirects the standard ouput from the preceding command and sends it to a file you give it
 - also overwrites
- `>>` appends the redirected input line/creates
- `2>` is standard error as output
- `<` gets an input from a location, instead of standard input
- `<` always points to command
- `>` always points to command
- `2>/dev/null` sending the error output to this is like a black hole 

--- 
## Random Commands to Sort or Keep into Random

- `man ls` is a manual about ls?
- `man man` is a manual on manual
- `echo $$` prints current process id
- `whereis` find a where a command is 
- `chmod` used for changing permissions 
  - `chmod u+x file1` gives the user execute permission for file1
  - `chmond u-rw file1` this removes user read and write permissions for file1
- `./` The dot means from current directory
- An alias is a user defined "shortcut" for a command, and lives in current memory or `.bashrc` (or `.bash_profile`) (I CANNOT REMEMBER, REMEMBER TO CLARIFY)



---

## To make directory hierarchies in one command
  - `mkdir -p books/{fiction,non-fiction}`
    - This command creates the following directories 
      - books
      - books/non-fiction
      - books/fiction
    - the `-p` option allows the creation of sub directories 
    - and the `{}` allows to list all the subdirectories in one command 
    - Spaces are important, don't put any spaces within the `{}` unless the file name have any but they shouldn't 
  - `mkdir -p movies/{action,drama,comedy/{films,standup}}`
    - This is just deeper heirarchies made through a single command and makes 
      - movies
      - movies/action
      - movies/drama
      - movies/comedy/film
      - movies/comedy/standup
  - `mkdir -p {~/books/{fiction,non-fiction},~/movies/{action,drama,comedy/{films,standup}},~/music}`
    - This creates the whole file structure in one command 

---

## Security 

- `chmod XXX file1`
  - First X is for Owner 
  - Second X is for Group
  - Third X is for Others
  - Execute value is `1`
  - Write value is `2`
  - Read value is `4`
  - So Read + Write Value is `6`

- Ports
  - Port 22 is for the terminal to access a server
  - Port 80 is the main door (http), is what everyone sees
    - also port 443 is https 
  - 
---
## Notes To Sort 
- Clean up wording on the wildcard section 
- - Show me all files starting with a *capital letter and a number*? (NEED TO CONFIRM) 

