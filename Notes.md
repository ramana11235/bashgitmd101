
# Tasks-To-Do
1. Finish the commonly used functions exercise
2. Start/Complete the wildcards exercise
3. You need to add how to run scripts on servers 
---
# Temp Notes To Sort 


## creating a git link with bitbucket
   -   `git remote add origin <git:ssh>`
       -   You get the `git` and `ssh` from creating a new repo on bitbucket
   -   And then `git remote --v` to see if it knows where origin is

--- 
### Security and ports
- A reverse proxy setup where server access to the world (port 80) has been cut off, and another server (the proxy server) has been given inbound connection with port 80. The proxy server (which IS the reverse proxy) then routes the inbound traffic through another port (e.g. 8080) to our server. The reverse proxy and our server have separate ip addresses 
  - check filipes diagram on trello
---



## Script outline for installing paint js 

1. install git 
```bash
$ sudo yum -y install git
```
2. clone jspaint git repo in
```bash
$ git clone https://github.com/1j01/jspaint.git
```
3. cd into `jspaint` dir
```bash
$ cd ~/jspaint
```
4. install node.js (and update)
```bash
$ wget https://nodejs.org/dist/v14.16.1/node-v14.16.1-linux-x64.tar.xz
$ VERSION=v14.16.1
$ DISTRO=linux-x64
$ sudo mkdir -p /usr/local/lib/nodejs
$ sudo tar -xJvf node-$VERSION-$DISTRO.tar.xz -C /usr/local/lib/nodejs
```


5. Add to the env variable file `~/.bash_profile` with 

```bash
$ echo "# Nodejs
$ VERSION=v10.15.0
$ DISTRO=linux-x64
$ export PATH=/usr/local/lib/nodejs/node-$VERSION-$DISTRO/bin:$PATH'>>~/.bash_profile"
```


6. Then source it to update it 

```bash
$ source ~/.bash_profile
```

7. Check if the right version is installed 

```bash
$ node -v
```

8. set up reverse proxy 

```bash
$ http -M
$ cd /etc/httpd/conf.d
$ touch jspaint.conf
$ echo '<VirtualHost *:80>
$   ProxyPreserveHost On
$
$   ProxyPass / http://127.0.0.1:8080/
$   ProxyPassReverse / http://127.0.0.1:8080/
$ </VirtualHost>'>>jspaint.conf`

```
   

9. cd into `jspaint`
```bash
$ cd ~/jspaint
```
10. restart the apache server
```bash
$ sudo systemctl restart httpd
```
11. install the dependecies with node package manager
```bash
$ npm i 
```

12.  something about systemchk, absolutely clueless on this part
```bash
$ sudo sh -c "cat > /etc/init.d/jspaint <<_end
$ #!/bin/bash
$ #description: JSPaint web app
$ #chkconfig: 2345 99 99
$ case $1 in
$ 	'start')
$ 		cd /home/ec2-user/jspaint
$ 		nohup npm run dev &
$  		;;
$ 	'stop')
$ 		kill -9 $(ps -ef | egrep 'npm|node' | awk '{print $2}')
$ 		;;
$ esac
$_end"
```bash
13. make that file executable
```bash
$ sudo chmod +x /etc/init.d/jspaint
```

14.  check the config
```bash
$ sudo chkconfig --add jspaint
$ sudo systemctl enable jspaint
$ sudo systemctl start jspaint
```
 
 