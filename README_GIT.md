# Documenting GIT & BitBucket & Other

## Intro 
Git is a version control software. 
It manages and tracks versions so you don't end up with **Final_FINAL_FINAL_v3.docx**

It does the managing and tracking by creating time lines. 

---

## Git Commands

```
# To start a repo
$ git init
# This initalises a 'Master Branch'

# To check the status of a git
$ git status

# To add the files for commit
$ git add . #This adds all the files in the entire directory

# For separate files (e.g. file1) getting ready to commit
$ git add file1

# To actually commit the files to timeline, with -m for informative message
$ git commit -m "

# To go to previous commit with hash 599f...
$ git checkout 599f...
# You can find this hash from command `git log`
```

## To Push To Repo

```bash
$ git push origin master
```

This pushes your `master` branch to the `origin` server


--- 
## Notes to sort 
- You push your git branch to bitbucket with a SSH key using Terminal