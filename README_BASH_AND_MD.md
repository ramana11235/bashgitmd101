# Bash and Markdown 101

This repo is our documentation on bash and markdown.This document itself acts as a markdown example with lists, numbered lists, and other formatting used.

This document also has information on Bash including a list of main bash commands.

---

We will learn:
- Bash 
- Markdown
  
  
By the way, this is a list:
  1. Number 1
  2. Number 2

--- 

## Bash 

Bash is a language that speaks with a Kernel. Used throughout linux machines around ~90%.

---

## List of Main Bash Commands

 

#### *Where Am I?* | Output shows current location

     $ pwd 
     >/user/path/location

#### *Where Can I Go?* | Output shows `directory_a`, `directory_b`, and `file1`

    $ls 
    > directory_a, directory_b, file1

#### *Showing hidden file* | Output shows `.hiddenfile`

    $ls -a
    > directory_a, directory_b, file1, .hiddenfile

#### *Go Somewhere* | Output changes current location into `examplefile1`
    
    $cd examplefile1

#### *Go Up* | Goes up a level 

    $cd ..

#### *Go Back* | Takes you to the previous directory (like a back button)

    $cd -

#### *Remove a file* | Removes `examplefile1`

    $rm examplefile

#### *Remove a directory* | Removes `exampledir`

    $rmdir exampledir 
    or
    $rm -rf exampledir

#### *Play Creationalist* | Creates a file called `newfile`

    $touch newfile

#### *Print to console* | Prints hi to console

    $echo hi
    > hi

#### *Truncating* | Truncates hi into `file`

     $echo 'hi' > file

#### *Printing the file* | In this case `file` contains hi

    $cat file 
    >hi

#### *Appending new line to* `file`

     $echo 'hello' > >file
     >hi
     >hello

#### *To Terminate Process*

    Ctrl + C

#### *End of Input*

    Ctrl + D 

--- 
