# DevOps

- Need to learn the problems faced which prompted the need for DevOps
  
  - Software development was generally a long time (and money) consuming venture. Especially in the early days with physical distribution...
  
    1. 1 year development
    2. 6 months testing
    3. 3 months production and distribution
    4. And then patching forever


  - As time went on, around mid 2000s, the internets infrastructure improved alongside with the complexity of software development projects increasing.
  
    - With this increase in project complexity:
  
      - It was harder to predict how long things took
      - The delivery time was increased to account for unknowns
      - There was a change management costs
        - Where specs were locked in and there was a cost to change them

  - This method had different outcomes for different stakeholders
    - Product Owners
      - The ones wanting the product wait ~2 years to see the product after sales and signoff
      - Any changes they might want can't be made without paying more
      - Are Sad
    - Delivery Managers
      - The one's acting as a bridge between Developers and Product owners and managing expectations
      - Things alywas go wrong, cream work, estimate 6 months and takes a year.
      - Are Sad
    - Developers/Engineers
      - Spend time on features and things customers don't see
      - They spend their time on old code (if they build a feature, it takes a year for it to go to testing and come back)
      - Can't rely on DM 
      - Are generally Sad to 
    - Customers
      - They just vibin, unaware, getting half done projects paying the price 

- These problems led to the development and introduction of Agile
  - Starting with Just In Time (JIT) with a focus on continuous improvement 
    - This was happening in 90s production, introduced by Toyota
  - Also the LEAN startup
    - Assume it won't work
    - Fail Fast 
    - Keep low overhead
    - Go to sales and feeback on Day 1
    - Pivot and iterate
  - And AGILE 
    - An iterative way of developing software for small team to deliver products often
    - Manifesto: 
      - Individuals and interactions over processes and tools
      - Working software over comprehensive documentation
      - Customer collaboration over contract negotiation
      - Responding to change over following a plan

- This changed the old waterfall method to 1 month sprints.
  - These 1 month sprints includes Dev and Testing, involving all the shareholders for review and change 

---
# Notes on Scrum
- Scrum is an implementation of the AGILE workflow
  - What is AGILE?
    - It is an iterative way of working and developing software 
    - It follows 4 principles:
      - Individu...
- It focuses on having a SCRUM Board (Kanban) and has **Ceremonies** and **Artifacts**
  - What is a **Ceremony**
    - I think it's a regular ritual of practices which focuses on highlighting problems/successes and so on and so forth
    - The following are ceremonies:
      - A Standup is a ceremony
      - A Sprint Plan is a ceremony 
      - A retrospective is a ceremony 
  - What is an **Artifact**
    - Things that scrum.org came up with that exist in project management using Scrum and Agile
    - Things like:
      - User Stories
        - Within this there are acceptance criteria 
      - Burn-down and Burn-Up chart
      - DoD (Definition of Done)

- There are also the implementation of **Roles**
  - Developers
  - Testers
  - DevOps Eng
  - Scrum Master 
    - Makes sure the scrum methodology is followed
    - Removes the impeedement to work for the team
  - Product Owner 
    - Has ownership of project, could be external or internal
  
- **Sprint Planning**
  - Initial Standup
    - What we did yesterday
    - Any Blockers
    - What we're doing today
  - Groom the backlog and scrum board
  - As a team plan the next sprint and the week that needs to get done 
  - The first sprint planning also includes **Prioritization**
  - **Planning Poker** - A grading of effort needed to complete each task by team
 
---


## Virtual Machines


- A virtual machine is a virtual computer within a computer
- The virtual machine can be set up with different resources compared to the host machine (limited by the host machines resources)
  - Things like Disk Space, RAM, CPU Power can be customised 
- Also allows the creation of different OS Systems 
  - Such as CentoOS, Ubuntu, REDhat, Genesis, Gameboy Colour, PS1, PS2, or just MacOS or Windows 
- This allows for flexibility, safety, and standardization

      - SIDENOTE:
        - DIFFERENCE BETWEEN EMULATORS AND VM 
        - VM doesn't have to do a translation between command and hardware
        - Emulator is a process in the operating system and does translations and is slower

- VirtualBox is one solution to create virtual machines 
- Vagrant declaratively uses VirtualBox (or others) to create a virtual machine 

---

# Notes To Order
- Silos of teams with cut off communication introduced problems
  - One of them is "it works on my machines", this is solved by virtualisation
    - A virtual machine standardises an environment for all machines to run and test the software on
- Read new relics what is devops 
- Clean up wording and add an intro and make more readable



